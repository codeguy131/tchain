use rpassword::prompt_password_stdout;
use tchain::config::*;
use tchain::wallet::prelude::*;
use zeroize::Zeroize;

fn dummy_main(remote: &str) -> Result<(), Box<dyn std::error::Error>> {
    // Get password from user
    let password =
        prompt_password_stdout("Enter password for key: ").expect("Problem reading password");

    let wallet = wallet_setup(password.as_str(), remote)?;

    let w_sender = wallet.sender.clone();

    let loc_handle = std::thread::spawn(move || {
        let mut buff = String::new();
        loop {
            buff.clear();
            std::io::stdin().read_line(&mut buff).unwrap();

            match buff.trim() {
                "exit" => {
                    w_sender.send(buff.to_owned()).unwrap();
                    println!("Exiting now...");
                    return;
                }
                _ => {
                    w_sender.send(buff.to_owned()).unwrap();
                    match w_sender.send(buff.to_owned()) {
                        Ok(_) => {
                            println!("Sent command: {}", buff.trim());
                        }
                        Err(e) => {
                            logger(format!("problem sending command{}", e).as_str());
                            return;
                        }
                    };
                }
            }
        }
    });

    while let Ok(msg) = wallet.receiver.recv() {
        match wallet.handle_local_command(msg) {
            Ok(_) => (),
            Err(e) => {
                logger(format!("Problem with local wallet command: {}", e).as_str());
                continue;
            }
        }
    }

    loc_handle.join().unwrap();

    password.to_owned().zeroize();
    Ok(())
}

fn main() {
    // Build the runtime
    let rt = std::thread::Builder::new()
        .name("wallet_thread".to_string())
        .spawn(|| {
            // Get damon address from args
            let addr = std::env::args()
                .nth(1)
                .unwrap_or_else(|| MAIN_LISTEN_ADDR.to_string());

            dummy_main(addr.as_str()).expect("Problem starting main")
        })
        .unwrap();

    rt.join().unwrap();
}
