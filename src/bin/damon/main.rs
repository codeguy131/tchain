use std::io::Write;

use rpassword::prompt_password_stdout;
use tokio::sync::mpsc;

use tchain::config::*;
use tchain::crypto::certs::*;
use tchain::damon::prelude::*;
use zeroize::Zeroize;

async fn dummy_main(
    _addr: &str,
    testing: Option<bool>,
    _kwargs: Vec<String>,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut password = prompt_password_stdout("Enter password: ")?;

    // Get key or genreate new
    let _p_key = gen_key(&password).unwrap();

    let (tx, rx) = mpsc::channel::<String>(48);
    let mut dam = Damon::default(
        match testing {
            Some(_) => true,
            None => false,
        },
        rx,
    )
    .await;
    logger("Damon started");

    // Create process for local damon handler
    let loc_handle = tokio::spawn(async move {
        let mut in_buff = String::new();
        loop {
            in_buff.clear();
            print!("Enter damon command: ");
            std::io::stdout().flush().unwrap();
            std::io::stdin().read_line(&mut in_buff).unwrap();
            match in_buff.trim() {
                // Handle exit command
                "exit" => match tx.send(in_buff.to_string()).await {
                    Ok(_) => {
                        println!("Sent exit command");
                        return;
                    }
                    Err(e) => panic!("Problem sending exit command: {}", e),
                },

                // Anything other than exit
                _ => match tx.send(in_buff.to_owned()).await {
                    Ok(_) => {
                        println!("Sent: {}", in_buff);
                        sleep_short().await;
                    }
                    Err(e) => {
                        println!("Problem sending command: {}", e);
                        return;
                    }
                },
            }
            sleep_short().await
        }
    });

    // Local command handler
    let loc_command = tokio::spawn(async move {
        while let Some(msg) = dam.receiver.recv().await {
            match dam.handle_local_command(msg.as_str()).await {
                Ok(_) => (),
                Err(e) => {
                    logger(format!("Problem handeling local command: {}", e).as_str());
                    continue;
                }
            };
        }
    });

    let futs = tokio::try_join!(loc_command, loc_handle);

    match futs {
        Ok(_) => (),
        Err(e) => panic!("{}", e),
    }

    // Zero out password when returning
    Ok(password.zeroize())
}

fn main() {
    use std::env::args;

    // Create thread for damon process
    std::thread::Builder::new()
        .name("damon_thread".to_string())
        .spawn(|| {
            // Initialize configured async runtime
            let drt = setup_damon_runtime().unwrap();
            drt.block_on(async move {
                if let Some(ar) = args().nth(1) {
                    logger("RUNNING IN TESTING MODE");
                    dummy_main(ar.as_str(), Some(true), vec![args().collect()])
                        .await
                        .unwrap();
                } else {
                    dummy_main(MAIN_LISTEN_ADDR, None, vec![args().collect()])
                        .await
                        .unwrap();
                }
            });
        })
        .unwrap()
        .join()
        .unwrap();
}
