use std::io::Write;
use std::net::TcpStream;
use std::sync::mpsc;

use super::wallet::*;
use crate::config::*;
use crate::crypto::certs::*;

impl WalletCommands {
    /// Command handler
    pub fn handle(&self) {
        match *self {
            WalletCommands::Exit() => {
                logger("Got exit command. Exiting now!!");
                std::process::exit(0);
            }

            WalletCommands::FindTX() => logger("Unimplemented"),

            WalletCommands::SendTX() => logger("Unimplemented"),

            WalletCommands::Connect(_addr) => logger("Unimplemented"),

            WalletCommands::InvalidCommand() => {
                logger("Invalid command");
            }
        }
    }
}

impl Wallet {
    pub fn default(passphrase: &str) -> Self {
        // Check for key directory
        check_for_main_dir(KEYS_DIR);

        // Genrate key if none exist
        let p_key = gen_key(passphrase).unwrap();

        // Message receiver
        let (tx, rx) = mpsc::channel::<String>();

        Self {
            inputs: vec![],
            outputs: vec![],
            balance: 0,
            key: p_key,
            receiver: rx,
            sender: tx,
        }
    }

    pub fn handle_local_command(&self, cmd: String) -> Result<(), Box<dyn std::error::Error>> {
        match cmd.trim() {
            "exit" => WalletCommands::Exit().handle(),
            "connect" => WalletCommands::Connect(None).handle(),
            "transfer" => WalletCommands::SendTX().handle(),
            "show_tx" => WalletCommands::FindTX().handle(),
            _ => WalletCommands::InvalidCommand().handle(),
        };
        std::thread::sleep(std::time::Duration::from_micros(1));
        Ok(())
    }
}

///
/// Creates and returns a wallet object
/// based on key
///
pub fn wallet_setup(passphrase: &str, addr: &str) -> Result<Wallet, Box<dyn std::error::Error>> {
    // Create stream for sending requests to damon
    let mut stream = TcpStream::connect(addr).expect("Problem connecting to damon");

    // Create empty wallet
    let wallet = Wallet::default(passphrase);
    assert!(wallet.key.rsa().is_ok());

    match stream.write_all("hello".as_ref()) {
        Ok(_) => (),
        Err(e) => {
            logger(format!("Problem writing hello to stream: {}", e).as_str());
            ()
        }
    }

    Ok(wallet)
}
