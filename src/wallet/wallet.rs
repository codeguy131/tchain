use crate::transactions::*;
use openssl::pkey::{PKey, Private};
use std::sync::mpsc;

///
/// Functions related to wallet commands
///
pub enum WalletCommands {
    Exit(),
    SendTX(),
    Connect(Option<*const str>),
    FindTX(),
    InvalidCommand(),
}

pub struct Wallet {
    pub inputs: Vec<TxInput>,
    pub outputs: Vec<TxOutput>,
    pub balance: u32,
    pub key: PKey<Private>,
    pub receiver: mpsc::Receiver<String>,
    pub sender: mpsc::Sender<String>,
}
