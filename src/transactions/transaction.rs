pub use serde::{Deserialize, Serialize};
use std::time::Duration;

#[derive(
    Debug, Default, Copy, Clone, Hash, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize,
)]
pub struct TxInput {
    sig: u8,
}

pub struct TxOutput {
    sig: u8,
}

///
/// Keeps the hash of all transaction data
/// as well as other things for finding
/// specific transactions
#[derive(Debug, Hash, Default, PartialEq, Eq, PartialOrd, Ord, Clone, Serialize, Deserialize)]
pub struct TransactionHeader {
    pub tx_hash: String,
    pub timestamp: Duration,
    pub sender: String,
    pub receiver: String,
    pub confirmations: u32,
}

///
/// The vehicle for transacting value on
/// the blockchain
///
#[derive(Debug, Hash, Default, PartialEq, Eq, PartialOrd, Ord, Clone, Serialize, Deserialize)]
pub struct Transaction {
    pub header: TransactionHeader,
    pub data: Vec<u8>,
}

impl Transaction {
    pub fn default() -> Self {
        Self {
            header: TransactionHeader {
                confirmations: 0,
                ..Default::default()
            },

            data: vec![],
        }
    }
}
