use crate::blocks::{load_blockchain, Blockchain};
use crate::config::*;
use std::collections::HashMap;
use std::net::SocketAddr;
use std::sync::Arc;
use tokio::net::TcpListener;
use tokio::sync::{
    mpsc::{Receiver, Sender},
    Mutex,
};

///
/// Screw it! Its a damon
///
/// Will handle input from local
/// and/or remote clients(peers)
/// concurrently if needed
///
#[derive(Debug)]
pub struct Damon {
    ///
    /// Loaded from LMDB database
    ///
    pub blockchain: Blockchain,

    ///
    /// List of active connections with a clone of
    /// the sender half of an mpsc channel
    ///
    pub peer_list: Arc<Mutex<HashMap<SocketAddr, Sender<String>>>>,

    ///
    /// Decides which blockchain to use
    ///
    pub testing: bool,

    ///
    /// Main receiver for damon IPC
    ///
    pub receiver: Receiver<String>,

    ///
    /// Listener for TCP connections
    ///
    pub srv: TcpListener,
}

impl Damon {
    pub async fn default(testing: bool, rx: Receiver<String>) -> Self {
        check_for_main_dir(DAEMON_DIR);

        // Load blockchain from file if exists
        let bc = match load_blockchain(testing) {
            // Match on result of loading blockchain
            Ok(b) => b,
            Err(e) => {
                logger(format!("Problem loading blockchain: {}", e).as_str());
                let b = Blockchain::default();
                b.write_blockchain().expect("Problem writing blockchain");
                drop(b);
                let f = load_blockchain(testing).expect("Problem loading blockchain");
                f
            }
        };

        let peers_buff: HashMap<_, Sender<String>> = HashMap::new();

        let srv = TcpListener::bind(MAIN_LISTEN_ADDR)
            .await
            .expect("Problem binding");

        Self {
            blockchain: bc,

            peer_list: Arc::new(Mutex::new(HashMap::from(peers_buff))),

            receiver: rx,

            testing,

            srv,
        }
    }
}
