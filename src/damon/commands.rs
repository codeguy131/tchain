use super::damon::Damon;
use super::local_handler::LocalCommands;
use crate::config::*;

impl Damon {
    ///
    /// Handle commands from local command handler
    ///
    pub async fn handle_local_command(
        &mut self,
        command: &str,
    ) -> Result<(), Box<dyn std::error::Error>> {
        match command.trim() {
            // Tell damon to shut down
            "exit" => LocalCommands::Exit().handle().await,

            // Get a copy of the blockchain from peers in peer list
            "sync_bc" => {
                let mut lock = self.peer_list.lock().await;
                let mut peers = Vec::new();
                for i in lock.iter_mut() {
                    peers.append(&mut vec![*i.0])
                }

                // tell damon to send "SYNC" request
                LocalCommands::SyncFromPeers(peers).handle().await;
            }

            // help menu
            "help" => {
                LocalCommands::Help().handle().await;
            }

            // Handles invalid commands
            _ => LocalCommands::InvalidCommand().handle().await,
        };

        sleep_short().await;
        Ok(())
    }
}
