use tokio::runtime::{Builder, Runtime};
use tokio::time::Duration;

///
/// Returns handle to an async runtime
///
pub fn setup_damon_runtime() -> Result<Runtime, Box<dyn std::error::Error>> {
    let rt = Builder::new_current_thread()
        .worker_threads(3)
        .max_blocking_threads(75)
        .enable_time()
        .enable_io()
        .thread_name("TCHAIN-DAMON-MAIN")
        .thread_keep_alive(Duration::from_secs(15))
        .build()
        .expect("Problem creating runtime");

    Ok(rt)
}
