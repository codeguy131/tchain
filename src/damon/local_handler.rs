use crate::config::{logger, sleep_short};
use crate::network::Connection;
use std::{io::Write, net::SocketAddr};
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpStream;

pub enum LocalCommands {
    Exit(),
    InvalidCommand(),
    SyncFromPeers(Vec<SocketAddr>),
    Help(),
}

impl LocalCommands {
    ///
    /// Handles commands received from controling process
    ///
    pub async fn handle(&self) {
        match self {
            // Kill the damon
            LocalCommands::Exit() => {
                logger("Got exit command. Exiting now...");
                std::process::exit(0)
            }

            // Handle invalid command
            LocalCommands::InvalidCommand() => {
                logger("Invalid command");
            }

            // Make a "SYNC" request to peer in damons peer list
            LocalCommands::SyncFromPeers(addr) => {
                logger("Syncing blockchain...");

                let stream = TcpStream::connect(&addr[..]).await.unwrap();
                let conn = Connection::default(stream).await;

                // Connect to damon
                let mut stream = conn.stream;

                // Write sync request to stream
                while let Err(e) = stream.write_all("SYNC".as_ref()).await {
                    logger(format!("Problem with SYNC request: {}", e).as_str());
                    match stream.flush().await {
                        Ok(_) => break,
                        Err(e) => {
                            logger(format!("Problem flushing stream: {}", e).as_str());
                            match stream.shutdown().await {
                                Ok(_) => continue,
                                Err(e) => {
                                    logger(format!("Problem shutting down stream: {}", e).as_str());
                                    std::process::exit(1);
                                } // shutdown error
                            } // Shutdown match
                        } // Flush error
                    } // Flush ok
                } // while end

                // Create buffers for stream;
                let mut in_buff = Vec::new();
                if let Err(e) = stream.read_exact(&mut in_buff).await {
                    logger(format!("Problem reading response: {}", e).as_str());
                }
            }

            // Display a menu with list of accepted commands
            LocalCommands::Help() => {
                std::io::stdout().flush().unwrap();
                println!(
                    "\
COMMANDS:
    sync_bc: sync blockchain from peers in peer_list
    exit: Close the damon process and handler\n"
                );
            }
        }
        sleep_short().await;
    }
}
