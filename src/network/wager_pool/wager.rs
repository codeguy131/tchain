use serde::{Deserialize, Serialize};

///
/// A wager is what gets sent to the wager pool
/// and gets randomly selected. The moner that signed the wager will
/// be the one who adds a block to the blockchain
///
#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Wager {
    pub sig: String,
    pub raw_wager: &'static [u8],
}

impl Wager {
    pub fn new() -> Self {
        return Self::default();
    }
}
