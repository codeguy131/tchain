use crate::config::logger;

pub enum WagerPoolEvents {
    ///
    /// Will tell whether pool is selecting a wager
    /// of not
    ///
    IsSelecting,
    ///
    /// Will return the last selected wager
    ///
    WagerSelected,
    ///
    /// Signals that a wager was added to the pool
    ///
    WagerAdded,
}

impl WagerPoolEvents {
    pub fn handle(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        match &self {
            WagerPoolEvents::IsSelecting => {
                logger("Wager pool selecting winner now!!");
            }
            WagerPoolEvents::WagerSelected => {
                // TODO: Implement wager pool selection process
                logger(format!("Wager selected: {:?}", ("Unimplemented")).as_str());
            }
            WagerPoolEvents::WagerAdded => {
                // TODO: Change
                logger("Unimplemented");
            }
        };

        Ok(())
    }
}
