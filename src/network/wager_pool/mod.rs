mod events;
mod wager;

use super::EntropyPool;
use crate::config::logger;
pub use events::WagerPoolEvents;
use rand::Rng;
use std::sync::{Arc, Mutex};
pub use wager::Wager;

///
/// Place to store wagers from miners
///
#[derive(Debug, Clone)]
pub struct WagerPool {
    rand_pool: EntropyPool,
    pub pool: Arc<Mutex<Vec<Wager>>>,
    pub size: usize,
}

impl WagerPool {
    pub fn new() -> Self {
        Self {
            rand_pool: EntropyPool::new(),
            pool: Arc::new(Mutex::new(Vec::new())),
            size: 0,
        }
    }

    ///
    /// Randomly selects winning wager from wager pool
    ///
    pub fn select_wager(mut self) -> Result<Wager, Box<dyn std::error::Error>> {
        WagerPoolEvents::IsSelecting.handle()?;

        let lock = self.pool.lock().unwrap();
        logger("Wager pool locked for selection");
        let wagers = lock.iter();
        let range = 0..wagers.count();
        let selected: usize = self.rand_pool.rng.gen_range(range);
        let winner: Wager = lock.clone()[selected].clone();
        drop(lock);
        WagerPoolEvents::WagerSelected.handle()?;
        Ok(winner)
    }
}
