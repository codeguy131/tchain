use tokio::io::{AsyncWriteExt, BufWriter};
use tokio::net::TcpStream;

///
/// A wrapper struct for a TCP connection
///
#[derive(Debug)]
pub struct Connection {
    pub stream: BufWriter<TcpStream>,
}

impl Connection {
    pub async fn default(stream: TcpStream) -> Connection {
        return Self {
            stream: BufWriter::new(stream),
        };
    }

    pub async fn send(&mut self, msg: String) -> Result<(), Box<dyn std::error::Error>> {
        self.stream.write(msg.as_bytes()).await.unwrap();
        Ok(())
    }
}
