use rand::prelude::*;

#[derive(Debug, Clone)]
pub struct EntropyPool {
    pub rng: StdRng,
}

impl EntropyPool {
    pub fn new() -> Self {
        let rng = StdRng::from_entropy();

        Self { rng }
    }
}
