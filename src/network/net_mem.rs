use super::EntropyPool;
use super::WagerPool;

///
/// Dependency injection for network resources such as
/// entropy source for selecting wagers randomly from
/// wager pool
///
#[derive(Debug, Clone)]
pub struct ResourcePool {
    ///
    /// Has a seeded prng for wager selection
    ///
    pub entropy_pool: EntropyPool,
    ///
    /// Collection of wagers from miners
    ///
    pub wager_pool: WagerPool,
}

impl ResourcePool {
    pub fn new() -> Self {
        Self {
            entropy_pool: EntropyPool::new(),
            wager_pool: WagerPool::new(),
        }
    }
}
