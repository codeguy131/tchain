use super::ResourcePool;
use std::{
    collections::HashMap,
    sync::{mpsc, Arc, Mutex},
};

pub struct Node {
    peer_list: Arc<Mutex<HashMap<mpsc::Sender<String>, &'static ResourcePool>>>,
    resource_pool: ResourcePool,
}

impl Node {
    pub fn new() -> Self {
        let peer_list = Arc::new(Mutex::new(HashMap::new()));
        Self {
            resource_pool: ResourcePool::new(),
            peer_list,
        }
    }
}
