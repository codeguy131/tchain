pub mod logging;
pub mod testing;

pub use logging::*;
pub use testing::*;
use tokio::time::{sleep, Duration};

use std::io::ErrorKind;
use std::path::Path;

/// The main directory for everything needed by the program
pub const TCHAIN_DIR: &str = ".tchain_stuff";

/// Directory created for testing
pub const TESTING_DIR: &str = "testing";

/// Directory for generated keys
pub const KEYS_DIR: &str = "keys";

/// Private key file
pub const PRIVATE_KEY_FILE: &str = "privkey.pem";

/// Public key file
pub const PUBLIC_KEY_FILE: &str = "pubkey.pem";

/// X509 cert file
pub const X509_FILE: &str = "cert.pem";

/// Directory for lmdb
pub const DATABASE_DIR: &str = "blockchain_storage";

/// Damon directory
pub const DAEMON_DIR: &str = "node";

/// Testing database
pub const TEST_DATABASE: &str = "test_blockchaindb";

/// Production database
pub const MAIN_DATABASE: &str = "blockchaindb";

/// Address for server to bind
pub const MAIN_LISTEN_ADDR: &str = "0.0.0.0:20080";

/// Testing passphrase for keys
pub const TEST_PASSPHRASE: &str = "password123";

/// CA constants
pub const PK_NAME: &str = "TCHAIN";
pub const CA_FILE: &str = "CA.p12";

/// Max amount of peers in peer list
pub const MAX_PEER_LIST: usize = 1500;

/// Daemon IPC default address
pub const DAMON_IPC: u32 = 20081;

/// Create main tchain directory
pub fn check_for_main_dir(path: &str) {
    let n_path = Path::new(TCHAIN_DIR);
    let v_path = Path::new(path);
    match std::fs::read_dir(n_path) {
        Ok(_) => match std::fs::read_dir(n_path.join(v_path)) {
            Ok(_) => {
                true;
            }
            Err(er) => {
                if er.kind() == ErrorKind::NotFound {
                    std::fs::create_dir_all(n_path.join(path)).unwrap();
                    true;
                } else {
                    panic!("Problem with child directory")
                }
            }
        },
        Err(e) => {
            if e.kind() == ErrorKind::NotFound {
                std::fs::create_dir_all(TCHAIN_DIR).unwrap();
                std::fs::create_dir_all(n_path.join(v_path)).unwrap()
            } else {
                panic!("Problem creating main dir");
            }
        }
    }
}

pub async fn sleep_short() {
    return sleep(Duration::from_micros(1)).await;
}
