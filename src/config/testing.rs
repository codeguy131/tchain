use super::*;
use std::path::Path;

pub fn chdir_testing() {
    check_for_main_dir(TESTING_DIR);

    let test_path = Path::new(TCHAIN_DIR).join(TESTING_DIR);

    std::env::set_current_dir(test_path).unwrap();
}
