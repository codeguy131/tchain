use crate::config::{check_for_main_dir, TCHAIN_DIR};
use std::fs::File;
use std::fs::OpenOptions;
use std::io::Write;
use std::path::Path;
use std::thread::sleep;
use std::time::Duration;

pub const LOG_DIR: &str = "logs";
pub const MAIN_LOG_FILE: &str = "tchain_main.log";

pub fn logger(msg: &str) {
    let main_dir = Path::new(TCHAIN_DIR);
    let log_dir = main_dir.join(LOG_DIR);
    let logf = log_dir.join(MAIN_LOG_FILE);

    // Check for log directory
    check_for_main_dir(LOG_DIR);

    // Open file for log or create if needed
    let mut log_file: File = OpenOptions::new()
        .create(true)
        .read(true)
        .append(true)
        .open(logf)
        .expect("Problem creating/opening log file");

    // Get current timestamp
    let tn = chrono::Utc::now().naive_utc().to_string();

    let proc_id = std::thread::current().name().unwrap().to_string();

    // Format log string
    let log_str = String::from(format!(
        "{:?}: THREAD: {:?}: INFO: {}\n",
        tn,
        proc_id,
        msg.to_owned()
    ));

    println!("{}", log_str);
    sleep(Duration::from_millis(1));

    // Write log msg to file
    log_file
        .write(log_str.as_ref())
        .expect("Problem writing to log file");
    drop(log_file);
}
