use crate::config::*;
use openssl::pkey::{PKey, Private};
use openssl::rsa::Rsa;
use openssl::symm::Cipher;
use std::fs;
use std::io::{ErrorKind, Result};
use std::path::Path;
use zeroize::Zeroize;

///
/// Generate a new 4096 bit public/private key pair using EVP
///
pub fn gen_key(passphrase: &str) -> Result<PKey<Private>> {
    // Check for the main directory
    check_for_main_dir(KEYS_DIR);

    let key_path = Path::new(TCHAIN_DIR).join(KEYS_DIR);
    let key_file = key_path.join(PRIVATE_KEY_FILE);
    let pub_key_file = key_path.join(PUBLIC_KEY_FILE);

    // Check for key file
    match fs::File::open(key_file.to_owned()) {
        // Exit if key exists
        Ok(_) => {
            let key =
                PKey::private_key_from_pem_passphrase(&fs::read(key_file)?, passphrase.as_ref())?;
            passphrase.to_owned().zeroize();
            return Ok(key);
        }

        // Create key if not found
        Err(e) => {
            if e.kind() == ErrorKind::NotFound {
                // Generate 4096 bit RSA private key
                let priv_key = Rsa::generate(4096)?;

                // Assign new key to PKey
                let p_key = PKey::from_rsa(priv_key)?;

                // Write new key to file
                fs::write(
                    key_file,
                    &p_key.private_key_to_pem_pkcs8_passphrase(
                        Cipher::aes_256_cbc(),
                        passphrase.as_bytes(),
                    )?,
                )?;

                // Write public key to file
                fs::write(pub_key_file, p_key.public_key_to_pem()?)?;
                passphrase.to_owned().zeroize();
                return Ok(p_key);
            } else {
                passphrase.to_owned().zeroize();
                panic!("Problem with key file: {}", e)
            }
        }
    }
}
