//!
//! A blockchain project with a smaller footprint.
//!
//! Aims to prove the effectiveness of a "proof of selection"
//! mining algorithm in order to make a more efficient, and fair
//! means of exchange for all.
//!
//! In essence, The miners will generate a "wager" and submit to
//! the wager pool. A wager is selected at "random" from the pool
//! and the miner associated with it then gets to add the block to
//! the chain
//!
//!
pub mod blocks;
pub mod config;
pub mod crypto;
pub mod damon;
pub mod network;
pub mod transactions;
pub mod wallet;

pub trait Testing {
    fn testing(&self) -> Self;
}
