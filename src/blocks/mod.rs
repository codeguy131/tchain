//!
//! Contians structs and methods for
//! Interacting with the blockchain
//!

mod block;
mod chain;
mod storage;

pub use block::Block;
pub use chain::Blockchain;
pub use storage::*;

use crate::Testing;
