extern crate heed;

use super::chain::Blockchain;
use crate::config::*;
use heed::types::*;
use heed::{Database, EnvOpenOptions};
use std::io::ErrorKind;
use std::path::Path;

impl Blockchain {
    pub fn write_blockchain(&self) -> heed::Result<()> {
        check_for_main_dir(DATABASE_DIR);

        let db_env = get_database_connection(match self.testing {
            true => true,
            false => false,
        })?;

        let db: Database<Str, SerdeBincode<Blockchain>> = db_env.create_database(None)?;

        let mut tx = db_env.write_txn()?;

        db.put(&mut tx, "main", self)?;

        tx.commit()?;

        Ok(())
    }
}

pub fn load_blockchain(testing: bool) -> heed::Result<Blockchain> {
    let db_env = get_database_connection(match testing {
        true => true,
        false => false,
    })?;

    let db: Database<Str, SerdeBincode<Blockchain>> = db_env.create_database(None)?;
    let tx = db_env.read_txn()?;

    let blockchain: Blockchain = match db.get(&tx, "main")? {
        Some(bc) => bc,
        None => {
            println!("Problem getting blockchain from database");
            let bc = Blockchain::default();
            bc
        }
    };

    Ok(blockchain)
}

fn get_database_connection(testing: bool) -> heed::Result<heed::Env> {
    check_for_main_dir(DATABASE_DIR);
    let main_path = Path::new(TCHAIN_DIR).join(DATABASE_DIR);
    let env_path = main_path.to_owned().join(match testing {
        true => TEST_DATABASE,
        false => MAIN_DATABASE,
    });

    match env_path.exists() {
        true => (),
        false => {
            std::fs::create_dir_all(env_path.to_owned())
                .expect("Problem creating database directory");
        }
    };

    match std::fs::read_dir(env_path.to_owned()) {
        Ok(_) => (),

        Err(e) => {
            if e.kind() == ErrorKind::NotFound {
                std::fs::create_dir_all(env_path.to_owned())?;
            } else {
                panic!("Problem with path: {}", e)
            }
        }
    }
    let env = EnvOpenOptions::new().open(env_path.to_owned().as_path())?;

    Ok(env)
}
