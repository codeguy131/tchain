use super::block::*;
use super::Testing;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

///
/// Uses a HashMap of blocks
/// along with the hashed data
/// to ensure immutability and
/// availability once distributed
///
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Blockchain {
    pub tip: Block,
    pub map: HashMap<u32, Block>,
    pub testing: bool,
}

///
/// Blockchain methods
///
impl Blockchain {
    pub fn default() -> Self {
        Self {
            tip: Block::default().hash_block(),
            map: HashMap::<u32, Block>::new(),
            testing: false,
        }
    }

    ///
    /// Gets the last block mined from the network
    /// and checks to see if it is already saved.
    /// If not saved, will grab from peer in
    /// peer list
    ///
    pub fn get_tip(&self) -> Result<Block, std::io::Error> {
        // Some stuff to get tip from network
        let block = Block::default().hash_block();
        // More to come

        Ok(block.to_owned())
    }

    ///
    /// Add block if wager is selected
    ///
    pub fn add_block(&mut self) -> std::io::Result<Self> {
        // Block template
        let new_block = &mut Block {
            header: BlockHeader {
                index: self.tip.header.index,
                proof: "".to_string(),
                block_hash: "".to_string(),
                previous_block_hash: self.tip.header.block_hash.clone(),
                time_mined: std::time::UNIX_EPOCH.elapsed().unwrap(),
                mainchain: match self.testing {
                    true => false,
                    false => true,
                },
                ..Default::default()
            },
            tx_list: vec![],
        };

        new_block.hash_block();

        self.to_owned()
            .map
            .insert(new_block.header.index, new_block.to_owned());

        // Replace tip with new block
        self.tip = new_block.clone();
        assert!(self.tip.header.block_hash != "".to_string());

        // TODO: Announce new tip to network

        Ok(self.to_owned())
    }

    ///
    /// Print out contents of blockchain to screen
    ///
    pub fn output_blockchain(self) -> std::io::Result<()> {
        let ser = serde_json::ser::to_string(&self)?;
        println!("{}", ser);
        Ok(())
    }
}

impl Testing for Blockchain {
    fn testing(&self) -> Self {
        self.to_owned().testing = true;
        self.to_owned()
    }
}
