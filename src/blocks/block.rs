use super::Testing;
use crate::transactions::*;
use serde::{Deserialize, Serialize};
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
use std::time::Duration;

///
/// Holds info related to the block
/// such as index and hashed block data
///
#[derive(Debug, Default, Hash, PartialEq, Ord, Eq, PartialOrd, Clone, Serialize, Deserialize)]
pub struct BlockHeader {
    pub index: u32,
    pub proof: String,
    pub valid: bool,
    pub block_hash: String,
    pub previous_block_hash: String,
    pub time_mined: Duration,
    pub mainchain: bool,
    pub next: u32,
}

///
/// Holds a list of transactions
/// and are added to the blockchain
/// by the miners (Coming soon)
///
#[derive(Debug, Hash, Ord, PartialEq, Eq, PartialOrd, Clone, Serialize, Deserialize)]
pub struct Block {
    pub header: BlockHeader,
    pub tx_list: Vec<Transaction>,
}

///
/// Block methods
///
impl Block {
    ///
    /// Creates a blank block
    ///
    pub fn default() -> Self {
        Self {
            header: BlockHeader {
                block_hash: String::from(""),
                mainchain: true,
                ..Default::default()
            },
            tx_list: Vec::new(),
        }
    }
    ///
    /// Will hash everything in the block
    /// and put it into the the block_hash
    /// field in the header
    ///
    pub fn hash_block(&mut self) -> Self {
        // Initialize hasher
        let hasher = &mut DefaultHasher::new();

        // collect hashed data into array
        Hash::hash(
            &[self.tx_list.hash(hasher), self.header.hash(hasher)],
            hasher,
        );

        // Assign finished hashed object to block_hash
        self.header.block_hash = hasher.finish().to_string();

        // Make sure not still default value
        assert!(self.header.block_hash != String::from(""));

        self.to_owned()
    }
}

impl AsRef<[u8]> for Block {
    fn as_ref(&self) -> &[u8] {
        unsafe {
            ::std::slice::from_raw_parts(
                (self as *const Block) as *const u8,
                ::std::mem::size_of::<Block>(),
            )
        }
    }
}

impl Iterator for Block {
    type Item = (*const [u8], *const [u8]);

    fn next(&mut self) -> Option<Self::Item> {
        let new_next = self.to_owned().header.index + self.header.next;
        self.header.index = self.header.next;
        self.header.next = new_next;
        Some((&self.header.to_owned().index.to_le_bytes(), self.as_ref()))
    }
}

impl Testing for Block {
    fn testing(&self) -> Self {
        self.to_owned().header.mainchain = false;
        self.to_owned()
    }
}
