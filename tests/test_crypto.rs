#[cfg(test)]
use tchain::config::*;
use tchain::crypto::certs::gen_key;

#[test]
fn test_key_gen() -> std::io::Result<()> {
    let _key = gen_key(TEST_PASSPHRASE)?;
    Ok(())
}
