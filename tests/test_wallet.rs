#[cfg(test)]
use tchain::config::*;
use tchain::wallet::prelude::*;

#[test]
fn test_new_wallet() -> Result<(), std::io::Error> {
    let _new_wallet = Wallet::default(TEST_PASSPHRASE);

    Ok(())
}
