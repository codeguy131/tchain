#[cfg(test)]
use tchain::blocks::*;

#[test]
fn test_new_block() -> std::io::Result<()> {
    let _new_block: Block = Block::default();
    Ok(())
}

#[test]
fn test_storage() -> heed::Result<()> {
    let mut bc = Blockchain::default();
    bc.testing = true;
    let block = Block::default();
    bc.map.insert(block.to_owned().header.index, block);

    bc.write_blockchain()?;

    let mut new_bc = load_blockchain(true)?;
    new_bc.add_block()?;
    assert!(new_bc.tip.header.block_hash != "".to_string());

    Ok(())
}
