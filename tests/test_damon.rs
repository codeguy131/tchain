#[cfg(test)]
use tchain::{config::*, damon::prelude::*};
use tokio::{io::AsyncWriteExt, sync::mpsc};

#[tokio::test]
async fn test_damon() -> tokio::io::Result<()> {
    let (_tx, rx) = mpsc::channel(4);
    let mut dam = Damon::default(true, rx).await;
    dam.handle_local_command("exit").await.unwrap();
    assert!(dam.blockchain.testing == true);
    let mut conn = tokio::net::TcpStream::connect(MAIN_LISTEN_ADDR)
        .await
        .unwrap();
    conn.write_all("SYNC".as_bytes()).await.unwrap();
    tokio::task::yield_now().await;
    Ok(())
}

#[tokio::test]
async fn test_runtime() -> tokio::io::Result<()> {
    let new_rt = setup_damon_runtime().unwrap();
    new_rt
        .spawn(async move {
            let th_name = std::thread::current().name().unwrap().to_string();
            logger(th_name.as_str());
            assert_eq!("TCHAIN-DAMON-MAIN", th_name)
        })
        .await
        .unwrap();
    new_rt.shutdown_background();
    Ok(())
}
