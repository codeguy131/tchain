#[cfg(test)]
use tchain::config::*;

#[test]
fn test_logger() -> std::io::Result<()> {
    logger("Test log msg");
    Ok(())
}

#[test]
fn test_check_dir() -> std::io::Result<()> {
    check_for_main_dir(DAEMON_DIR);
    Ok(())
}
