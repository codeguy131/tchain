use tchain::network::{Wager, WagerPool};

#[test]
fn test_wager_pool_append() -> Result<(), std::io::Error> {
    let w_pool = WagerPool::new();
    {
        let mut lock = w_pool.pool.lock().unwrap();
        let wager = Wager::default();

        lock.append(&mut vec![wager])
    }
    Ok(())
}
