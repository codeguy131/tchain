FROM rust:latest as prebuild
COPY . /app
WORKDIR /app

RUN cargo build --release

FROM rust:slim-buster

COPY --from=prebuild /app/target/release/tchaind .

EXPOSE 20080

CMD ["./tchaind"]
