# T-CHAIN

[![pipeline status](https://gitlab.com/codeguy131/tchain/badges/main/pipeline.svg)](https://gitlab.com/codeguy131/tchain/-/commits/main)

[![coverage report](https://gitlab.com/codeguy131/tchain/badges/main/coverage.svg)](https://gitlab.com/codeguy131/tchain/-/commits/main)

## A blockchain using proof of selection for mining

The purpose of this project is a blockchain using a proof of selection algorithm
for adding blocks to the blockchain. My idea is for the miners to generate
(pseudo random or manual) "wagers".
The "wagers" are then submitted to a wager pool and a winning one is
selected at "random" using entropy from active nodes on the network
(Some kind of distributed entropy pool)
and the miner its attached to gets to add the block
and gets a token.

Monero donations:
83HkrMGqBnQMJZggQ9RpDr2fjYbcyMfDcZ1ZYPSqbduM89rpwdCP9mKMcKjGDCdEgN97GpACkLxMvS4fces5qWnhNX4cnuC
